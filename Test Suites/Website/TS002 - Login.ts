<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS002 - Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>de8c044d-7d5b-4586-828d-917df98523b3</testSuiteGuid>
   <testCaseLink>
      <guid>25ba39d3-4ec6-4b57-b3b1-c94e0b5ed215</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <iterationNameVariable>
         <defaultValue>findTestData('Website/WEBDT001 - Login By Filling Out The Form')</defaultValue>
         <description></description>
         <id>3031b556-53ce-4868-85b0-4a68439b1c82</id>
         <masked>false</masked>
         <name>password</name>
      </iterationNameVariable>
      <testCaseId>Test Cases/Monica Cindy/Website/Login/Data Binding/DT001 - Succed Login By Filling Out The Formm</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>434d103d-2745-498e-8166-bd08e4b5292f</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Website/DT001 - Login By Filling Out The Form</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>434d103d-2745-498e-8166-bd08e4b5292f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>email</value>
         <variableId>6bb2d00b-8ef2-413f-a724-aa17b51087f3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>434d103d-2745-498e-8166-bd08e4b5292f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>3031b556-53ce-4868-85b0-4a68439b1c82</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>91b677e7-399f-45f5-a5ef-62847f5372cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Monica Cindy/Website/Login/Negative/LG004 - Login With Invalid Email And Valid Password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1687af66-978f-40d7-8c25-71b2b66d5685</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Monica Cindy/Website/Login/Negative/LG005 - Login With Valid Email And Invalid Password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>627146ff-b939-4ab0-a018-adb39e010913</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Monica Cindy/Website/Login/Negative/LG006 - Login Without Submitting Email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e41d8778-abe7-4139-8e6e-0fd0f4c029c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Monica Cindy/Website/Login/Negative/LG007 - Login Without Submitting Password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ffa8ba10-0e19-4e49-bdd1-f60c6fd48f5d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Monica Cindy/Website/Login/Negative/LG009 - Forgot Password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f6964756-4a61-42d2-aec6-463c824c3189</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Monica Cindy/Website/Login/Negative/LG010 - Forgot Password - Sent Reset Link</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>fd7bec66-cd57-43a1-89c0-6f3383c9eca1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Monica Cindy/Website/Login/Negative/LG015 - Forgot Password - First Time Login After Changed Password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ed3b7e24-67a0-4c5c-b2e5-a53e94f06684</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Monica Cindy/Website/Login/Negative/LG016 - Login Without Anything</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>2ca9cd5b-be43-4824-ae20-ea2ca80abff3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Monica Cindy/Website/Login/Positive/LG001 - Succeed Open The Login Menu</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1a116f3c-a861-46e5-b357-bb5a22fb6dde</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Monica Cindy/Website/Login/Positive/LG003 - Succeed Login by Filling Out the Form</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>44249977-9a38-4aa7-bc5b-f5f8a091eeb4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Monica Cindy/Website/Login/Positive/LG008 - Login then Logout</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
