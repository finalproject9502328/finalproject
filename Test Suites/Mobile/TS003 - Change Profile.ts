<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS003 - Change Profile</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>38a24036-849a-4286-af64-a7be7d6c687b</testSuiteGuid>
   <testCaseLink>
      <guid>56bc91dc-3192-43ce-bb47-812c90dc5b82</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Monica Cindy/Mobile/Login/Positive/LG004 - Succeed Login by Filling Out the Form</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>c14d6846-2af3-4a72-abbd-facfdf42db5f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Monica Cindy/Mobile/Change Profile/Positive/CP001 - Succeed Open The Change Profile Menu</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>afb9c672-6655-40d4-94cb-3a4eef313e25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Monica Cindy/Mobile/Change Profile/Positive/CP006 - Change Profile with Birthday More Than 6 Years Old</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>9701c4cf-3301-4ee8-9197-fec7bc9a31b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Monica Cindy/Mobile/Change Profile/Positive/CP007 - Change Profile In FullName Field</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>983d2e00-6686-4787-aa68-669d4be114c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Monica Cindy/Mobile/Change Profile/Negative/CP010 - Change Profile By Symbols And Number</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f73bbb2a-0521-4665-b39d-54fe41ef02a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Monica Cindy/Mobile/Change Profile/Negative/CP012 - Change Profile By Removing Fullname</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4ffbac9f-1022-43d7-9200-a20ec13fd37f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Monica Cindy/Mobile/Change Profile/Negative/CP013 - Change Profile By Removing Birthdate</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
