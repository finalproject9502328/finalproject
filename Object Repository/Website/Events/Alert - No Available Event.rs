<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Alert - No Available Event</name>
   <tag></tag>
   <elementGuidId>b4b8340c-a44f-4f63-a500-be5e7c5f66d2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='containerEventInner']/h3</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#containerEventInner > h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>f96d5719-7358-4daa-a78a-a080b2a1901d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>No available event &quot; random &quot;</value>
      <webElementGuid>3acf6ed0-257e-4b7e-aecd-bab53e3bfa4c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;containerEventInner&quot;)/h3[1]</value>
      <webElementGuid>f0f0b5e1-b013-4d3e-85d4-9ec4c2878593</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='containerEventInner']/h3</value>
      <webElementGuid>c59b14d2-a8a7-4513-aaeb-e7083f15c5bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Events'])[4]/following::h3[1]</value>
      <webElementGuid>144c60a2-4ded-4f95-a0a1-2fbd5b791986</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CODING.ID Event'])[1]/following::h3[1]</value>
      <webElementGuid>e1d74fbe-0ff6-4c86-b412-b1793ccbd9ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gabung CODING.ID Newsletter'])[1]/preceding::h3[1]</value>
      <webElementGuid>c96b51bb-3249-435b-8739-b50c4f942c1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Format email tidak valid'])[1]/preceding::h3[2]</value>
      <webElementGuid>ce9e25e1-30f5-409e-915b-14ea4424a30a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='No available event &quot; random &quot;']/parent::*</value>
      <webElementGuid>81c5bcd0-a223-4f04-9756-78fd613612b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/h3</value>
      <webElementGuid>554e61eb-3c5c-46e6-8ae8-a79e753fd958</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'No available event &quot; random &quot;' or . = 'No available event &quot; random &quot;')]</value>
      <webElementGuid>3e11c47a-4131-42c9-8879-fe569c470251</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
