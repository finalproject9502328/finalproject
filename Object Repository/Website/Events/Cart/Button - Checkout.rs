<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button - Checkout</name>
   <tag></tag>
   <elementGuidId>7e1ae05c-ef1a-4296-89f0-565bdae2b210</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#checkoutButton</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='checkoutButton']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>517d0a3d-c3dd-4b66-8847-e3d75a973978</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>9c067ad0-b1f1-4977-9f89-be5b446bd316</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>checkoutButton</value>
      <webElementGuid>b296a1e6-4a0d-46c2-af00-4592be5e8cd8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Checkout</value>
      <webElementGuid>725ede7e-2e0a-4b17-a24c-328a04b03657</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkoutButton&quot;)</value>
      <webElementGuid>8250fb85-e45e-4c78-b889-abf11152fa0b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='checkoutButton']</value>
      <webElementGuid>04020e3f-b59d-429b-99b7-2e75deac0ff8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='cartForm']/div[2]/div/button</value>
      <webElementGuid>e79815f5-6f40-491f-9cfb-e88be8fb0a3a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 85.000'])[2]/following::button[1]</value>
      <webElementGuid>96c20dae-dec2-42dd-93e4-abe7b93ffd3a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total'])[1]/following::button[1]</value>
      <webElementGuid>aac7705c-4e01-442c-94b3-1adb025d0984</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apply'])[1]/preceding::button[1]</value>
      <webElementGuid>0550bb0f-fe28-4951-b36f-a2f3d9873327</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Riwayat Pembelian'])[1]/preceding::button[2]</value>
      <webElementGuid>3ca7e5a3-38ef-499f-b228-26080bd1f4dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/button</value>
      <webElementGuid>f1221c4d-36fc-4320-b3da-18ad102cc9ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'checkoutButton' and (text() = 'Checkout' or . = 'Checkout')]</value>
      <webElementGuid>f6d14b72-1b96-4d82-99c6-1f2e4c0d8c3e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
