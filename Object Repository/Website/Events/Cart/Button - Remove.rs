<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button - Remove</name>
   <tag></tag>
   <elementGuidId>2140e2b5-c71c-4e86-965e-280e81c85aa1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a > p</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='cartForm']/div/div/div[3]/a/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>d5c41d01-0494-43f4-867b-a2564dabdd77</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                    Remove </value>
      <webElementGuid>f9e9e216-092e-4e6b-a378-f2cbcdeea0a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cartForm&quot;)/div[@class=&quot;col-md-8&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-4&quot;]/a[1]/p[1]</value>
      <webElementGuid>65e5902b-bec2-4175-8f95-37fe2eeaf587</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='cartForm']/div/div/div[3]/a/p</value>
      <webElementGuid>efc3783e-d3e6-45a5-98be-7c9e732b4132</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Event'])[1]/following::p[3]</value>
      <webElementGuid>0813b37b-8e9c-476b-a461-a403f5a92623</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total'])[1]/preceding::p[1]</value>
      <webElementGuid>27e6ad5d-fdcb-4f5d-9300-fbba8107dfdb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 85.000'])[2]/preceding::p[1]</value>
      <webElementGuid>a3b95fa4-1499-4602-be43-5b87adf8a5bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Remove']/parent::*</value>
      <webElementGuid>47b8cced-3c86-49fe-a1e1-e4f5fcc39738</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/p</value>
      <webElementGuid>1302f3ed-f7df-4f68-b1e7-64247fcc8582</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = '
                                                    Remove ' or . = '
                                                    Remove ')]</value>
      <webElementGuid>a762a034-6aea-45a4-9d55-327386d25cc6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
