<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Payment - Danamon Online Banking</name>
   <tag></tag>
   <elementGuidId>e437ff98-7e96-4ecf-9648-dda2aef6664a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div:nth-of-type(7) > a.list > div.list-content > div.list-title.text-actionable-bold</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div/div[7]/a/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>7254f12a-a39b-4d8b-ad38-6d00e6a4f08a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-title text-actionable-bold</value>
      <webElementGuid>ed21c397-b20a-4523-9604-17c84bdb1ad2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Danamon Online Banking</value>
      <webElementGuid>92965c80-a2ad-4edd-b115-59df7ca55a7e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;payment-container-list&quot;]/div[7]/a[@class=&quot;list&quot;]/div[@class=&quot;list-content&quot;]/div[@class=&quot;list-title text-actionable-bold&quot;]</value>
      <webElementGuid>cbbbfcae-fdd8-4aaa-bbb6-820ddb2b3f77</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Website/Events/Checkout/iframe_concat(id(, , snap-midtrans, , ))_popup_1689793297466</value>
      <webElementGuid>50ea718f-f0ba-4886-af10-2c86a6ffbaa8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div/div[7]/a/div/div</value>
      <webElementGuid>4bb43bf0-c9df-4f78-a47a-f59989130ddb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OCTO Clicks'])[1]/following::div[7]</value>
      <webElementGuid>f9394944-73eb-4797-9600-00242dccfc7e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BCA KlikPay'])[1]/following::div[14]</value>
      <webElementGuid>cde854f3-d68c-40b7-bd06-3992c5b8eacd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Indomaret'])[1]/preceding::div[5]</value>
      <webElementGuid>d525e2a2-4f2e-49be-b1cb-fefab0203c33</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Akulaku PayLater'])[1]/preceding::div[13]</value>
      <webElementGuid>9ca6dc15-f8d4-4ac0-96b8-3374b4c312fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Danamon Online Banking']/parent::*</value>
      <webElementGuid>22464389-f273-4c66-93aa-ffc5b5a7f34e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/a/div/div</value>
      <webElementGuid>179bc321-2887-4b4e-9746-593c9905c499</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Danamon Online Banking' or . = 'Danamon Online Banking')]</value>
      <webElementGuid>ae144313-ab83-48e3-b460-fd2c0e47b67a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
