<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Creditdebit card</name>
   <tag></tag>
   <elementGuidId>c79cc4a5-6d12-4e4d-ada0-c01a95d5e64e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.title-text.text-actionable-bold</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div/div/div/span[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>40aee051-f97d-4271-b4fc-584eb09a4f6c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>title-text text-actionable-bold</value>
      <webElementGuid>b0b1f554-d753-4beb-91c2-2bcc6650bf21</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Credit/debit card</value>
      <webElementGuid>1ae6c440-4a7c-4e48-bc1b-ceee79586e63</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;credit-card__wrapper&quot;]/div[@class=&quot;credit-card__content&quot;]/div[@class=&quot;page-title&quot;]/span[@class=&quot;title-text text-actionable-bold&quot;]</value>
      <webElementGuid>e9ce6708-5366-4f2c-9777-fe95e9df039e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Website/Events/Checkout/iframe_concat(id(, , snap-midtrans, , ))_popup_1689793297466</value>
      <webElementGuid>f94d4d06-5d51-42b4-b320-8cd4046fdf23</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div/div/div/span[2]</value>
      <webElementGuid>b22dd6ae-f04f-4d93-9569-959df14cec92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order details'])[1]/following::span[2]</value>
      <webElementGuid>51e25bcf-e0a2-4221-8044-b439b55142cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Card number'])[1]/preceding::span[1]</value>
      <webElementGuid>ee7b435f-4f94-4417-a756-af242f1ba296</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Expiration date'])[1]/preceding::span[3]</value>
      <webElementGuid>5f64a012-97fc-4a3b-a1e5-0c9bb5693cbd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Credit/debit card']/parent::*</value>
      <webElementGuid>45bd255f-0597-47a4-863d-508b19e51b9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]</value>
      <webElementGuid>00075193-e93e-431d-9796-fef59f83db4e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Credit/debit card' or . = 'Credit/debit card')]</value>
      <webElementGuid>247f521f-4d99-445e-893d-0078c2e6b253</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
