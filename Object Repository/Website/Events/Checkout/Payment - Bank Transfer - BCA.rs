<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Payment - Bank Transfer - BCA</name>
   <tag></tag>
   <elementGuidId>9b68dd1a-97ab-46dd-b79b-914bacd0a382</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.payment-page-text</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div[2]/a/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>76acdca1-94f8-4175-ab95-08b3c14e94ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>payment-page-text</value>
      <webElementGuid>61d8c742-f824-496b-a29c-6af74516adc7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>BCA</value>
      <webElementGuid>420c830f-eec7-4b9f-9aa0-c3b9c6f46c33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;bank-list-layout vertical&quot;]/a[@class=&quot;bank-list&quot;]/div[@class=&quot;payment-page-text&quot;]</value>
      <webElementGuid>af2ff3be-9007-4d28-90e8-2d3e951da7ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Website/Events/Checkout/iframe_concat(id(, , snap-midtrans, , ))_popup_1689793833467</value>
      <webElementGuid>82cb9151-a3a2-434b-9b53-e5e62740e188</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div[2]/a/div</value>
      <webElementGuid>eb653548-07f5-4916-984a-3effac8016cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bank transfer'])[1]/following::div[2]</value>
      <webElementGuid>c35654d6-c263-4ee7-b1f0-3536ad098134</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mandiri'])[1]/preceding::div[5]</value>
      <webElementGuid>df110369-ab33-46d2-8b5b-dd26327e479d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div</value>
      <webElementGuid>9351adc7-a06d-464f-b0ab-16504d2f71f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'BCA' or . = 'BCA')]</value>
      <webElementGuid>fafb2d9f-027f-4219-8f6d-ad96254c572e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
