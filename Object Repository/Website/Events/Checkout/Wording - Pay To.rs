<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Wording - Pay To</name>
   <tag></tag>
   <elementGuidId>f32229db-d204-4516-a6cd-9d4607eed906</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Invoice Date'])[1]/following::div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>33904648-5980-4928-b603-9bdc66477bdf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                            Pay To
                            PT Dwi Data Talenta Prima
                            RUKO DALTON EXTENSION, SCIENTIA GARDEN, BLOK DLNT
                                NO.39 GADING SERPONG, TANGERANG.
                        
                    </value>
      <webElementGuid>010ba794-5bbd-4164-a1e7-22797bbbc6b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-htmlimports no-flash no-proximity no-applicationcache blobconstructor blob-constructor cookies cors customprotocolhandler dataview eventlistener geolocation history no-ie8compat json notification queryselector serviceworker customevent postmessage svg templatestrings typedarrays websockets no-xdomainrequest webaudio webworkers no-contextmenu cssall audio canvas canvastext contenteditable emoji olreversed no-userdata video no-vml webanimations webgl adownload audioloop canvasblending todataurljpeg todataurlpng todataurlwebp canvaswinding no-ambientlight hashchange inputsearchevent pointerevents no-hiddenscroll mathml unicoderange no-touchevents no-unicode no-batteryapi no-battery-api crypto no-dart gamepads fullscreen indexeddb indexeddb-deletedatabase intl pagevisibility performance pointerlock quotamanagement requestanimationframe raf vibrate no-webintents no-lowbattery getrandomvalues backgroundblendmode cssanimations backdropfilter backgroundcliptext appearance exiforientation audiopreload&quot;]/body[1]/div[@class=&quot;wm-main-wrapper&quot;]/div[@class=&quot;wm-main-section&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6 col-sm-12&quot;]/div[1]</value>
      <webElementGuid>94825665-f45c-4571-ad73-5fd934ed2f0a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invoice Date'])[1]/following::div[2]</value>
      <webElementGuid>10fe5c22-c664-4b6e-a931-2e8e0009784f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Billed To'])[1]/following::div[2]</value>
      <webElementGuid>a5f3a678-7318-4918-80ee-85cf2c7d55bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rangkuman Pembelian'])[1]/preceding::div[2]</value>
      <webElementGuid>479ab004-85d7-4cc2-ad2c-ba2ccc720f5a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[4]/div</value>
      <webElementGuid>319dee03-57ce-4d8a-8b0b-f9de27ee85d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        
                            Pay To
                            PT Dwi Data Talenta Prima
                            RUKO DALTON EXTENSION, SCIENTIA GARDEN, BLOK DLNT
                                NO.39 GADING SERPONG, TANGERANG.
                        
                    ' or . = '
                        
                            Pay To
                            PT Dwi Data Talenta Prima
                            RUKO DALTON EXTENSION, SCIENTIA GARDEN, BLOK DLNT
                                NO.39 GADING SERPONG, TANGERANG.
                        
                    ')]</value>
      <webElementGuid>c2d7f7de-7f60-4718-977f-6a5bfbef01b7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
