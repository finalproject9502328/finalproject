<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Wording - Bank Transfer - BCA</name>
   <tag></tag>
   <elementGuidId>3e0d9ec1-0e90-4321-8162-bf9c5c1aac6f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.payment-page-layout.payment-page-text.payment-page-main-instruction</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b7a10630-1e01-48fa-a466-84b8d981fc03</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>payment-page-layout payment-page-text payment-page-main-instruction</value>
      <webElementGuid>6aa3b063-18b8-4e18-8411-7492f862033b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Complete payment from BCA to the virtual account number below.</value>
      <webElementGuid>9d6208e7-1036-43f4-823e-4a115d0c9c92</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;payment-page&quot;]/div[1]/div[@class=&quot;payment-page-layout payment-page-text payment-page-main-instruction&quot;]</value>
      <webElementGuid>99fbc830-f822-4e5b-bfd3-778eda6bae6d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Website/Events/Checkout/iframe_concat(id(, , snap-midtrans, , ))_popup_1689793833467</value>
      <webElementGuid>804e928c-39c0-4caf-b605-f04575fe18dc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div/div/div[2]</value>
      <webElementGuid>773a5b00-d671-4fb3-b370-0143e8c86b01</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bank BCA'])[1]/following::div[1]</value>
      <webElementGuid>61917f7b-9a15-4957-8f33-643beb0915a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Virtual account number'])[1]/preceding::div[1]</value>
      <webElementGuid>eca478d6-72f4-4782-b8fd-298e75695da2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy'])[1]/preceding::div[3]</value>
      <webElementGuid>fd88c570-bd9c-4a27-8648-1747fdb2673e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Complete payment from BCA to the virtual account number below.']/parent::*</value>
      <webElementGuid>df6502a9-6219-4d81-a02c-8d2763396351</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/div/div[2]</value>
      <webElementGuid>df0730a9-7064-467c-b6f3-131bf98b1e24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Complete payment from BCA to the virtual account number below.' or . = 'Complete payment from BCA to the virtual account number below.')]</value>
      <webElementGuid>02f33541-f08d-4c3b-bf14-492b7c515e84</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
