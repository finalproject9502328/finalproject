<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Payment - BCA KlikPay</name>
   <tag></tag>
   <elementGuidId>a15c9ccd-00ea-4c08-9ed1-8c4c4f449c58</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div:nth-of-type(5) > a.list > div.list-content > div.list-title.text-actionable-bold</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div/div[5]/a/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3fabc88a-d570-492a-9e6a-5fde54e4f46c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-title text-actionable-bold</value>
      <webElementGuid>5a770959-839f-4f06-a524-8b5cd7e957a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>BCA KlikPay</value>
      <webElementGuid>86734c9e-ffdc-45dc-b0e6-77954c64497e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;payment-container-list&quot;]/div[5]/a[@class=&quot;list&quot;]/div[@class=&quot;list-content&quot;]/div[@class=&quot;list-title text-actionable-bold&quot;]</value>
      <webElementGuid>f23ee293-ab88-4b51-9519-a0e96888445f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Website/Events/Checkout/iframe_concat(id(, , snap-midtrans, , ))_popup_1689793297466</value>
      <webElementGuid>1798f7ac-30e4-4f28-b07c-c490eca938f5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div/div[5]/a/div/div</value>
      <webElementGuid>a00a61b7-02f5-41cb-b400-07708a4e5c9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ShopeePay'])[1]/following::div[8]</value>
      <webElementGuid>33dd3817-cf19-4b78-bafe-e6a96139e163</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bank transfer'])[1]/following::div[21]</value>
      <webElementGuid>3b8534c7-7a45-4266-b2be-8d929b69afb2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OCTO Clicks'])[1]/preceding::div[5]</value>
      <webElementGuid>5bdc464c-648d-490d-a414-f01f83479429</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Danamon Online Banking'])[1]/preceding::div[12]</value>
      <webElementGuid>b7cc0891-827e-4c02-a4bb-f173e2961ce0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='BCA KlikPay']/parent::*</value>
      <webElementGuid>8c55352c-0e60-4ae8-93ed-c385bba21820</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/a/div/div</value>
      <webElementGuid>a5ad59a6-a2a6-41e4-a0a2-aa2e7523b422</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'BCA KlikPay' or . = 'BCA KlikPay')]</value>
      <webElementGuid>fe40eb67-3405-4736-aa7b-9e4729dc415a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
