<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Wording - BCA KlikPay</name>
   <tag></tag>
   <elementGuidId>9c151a30-60db-42f0-a8b7-8d2b832a45f9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.payment-page-layout.payment-page-text</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3d39cbc0-b3ca-4d02-aa28-bb100a99d168</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>payment-page-layout payment-page-text</value>
      <webElementGuid>041fe072-8463-4f1c-bd6b-ee909bf9f6d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Please complete BCA KlikPay through BCA KlikPay website.</value>
      <webElementGuid>57d7548e-9e7b-4b15-8713-459cbea45067</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;payment-page-layout payment-page-text&quot;]</value>
      <webElementGuid>9a76df97-9ed6-4364-a42d-927548a9676c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Website/Events/Checkout/iframe_concat(id(, , snap-midtrans, , ))_popup_1689793297466</value>
      <webElementGuid>93cd57b6-06c7-478f-95de-f6f00b41bb28</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div[2]</value>
      <webElementGuid>557d9e51-40a8-40c0-8cd2-6b3ce7687f87</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BCA KlikPay'])[1]/following::div[1]</value>
      <webElementGuid>65ae4179-793d-4cc1-83ba-f707cc738dc2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='How to pay'])[1]/preceding::div[1]</value>
      <webElementGuid>bb9f3f18-dfbc-4b07-8424-50720197226f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Please complete BCA KlikPay through BCA KlikPay website.']/parent::*</value>
      <webElementGuid>4a253ee7-8f42-42bc-a3b1-9ee5d8cab1a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div[2]</value>
      <webElementGuid>fc359772-1879-4468-8e36-5dfcd115cd1c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Please complete BCA KlikPay through BCA KlikPay website.' or . = 'Please complete BCA KlikPay through BCA KlikPay website.')]</value>
      <webElementGuid>81710ca8-4a70-47fd-bf79-769183d0f717</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
