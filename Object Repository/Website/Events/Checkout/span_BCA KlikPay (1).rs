<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_BCA KlikPay (1)</name>
   <tag></tag>
   <elementGuidId>707e2434-a1a8-4277-a857-0e2ececec101</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.title-text.text-actionable-bold</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div/span[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>d1e6f361-3fa3-47a3-bc34-3961ee0a01b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>title-text text-actionable-bold</value>
      <webElementGuid>e158fec2-f787-497e-bcbb-0f5c7091e9d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>BCA KlikPay</value>
      <webElementGuid>edd38b19-c09f-483e-ac39-a4534667d5b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;page-title&quot;]/span[@class=&quot;title-text text-actionable-bold&quot;]</value>
      <webElementGuid>068e78ea-bf8e-4890-a94d-75331dcc2068</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Website/Events/Checkout/iframe_concat(id(, , snap-midtrans, , ))_popup_1689793297466</value>
      <webElementGuid>98959278-bc69-49c1-8adc-fe1c069cbe51</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div/span[2]</value>
      <webElementGuid>7c47c25b-09a6-4f76-b090-2d4dc0eebaa8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order details'])[1]/following::span[2]</value>
      <webElementGuid>6a2d1b94-5867-4c0a-886c-e1f6c4fb44a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please complete BCA KlikPay through BCA KlikPay website.'])[1]/preceding::span[2]</value>
      <webElementGuid>dc1d8000-fe0b-412b-a5dc-fb128147eea5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='How to pay'])[1]/preceding::span[2]</value>
      <webElementGuid>7e0e8114-b43b-4d69-a109-4c104063f337</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='BCA KlikPay']/parent::*</value>
      <webElementGuid>7a8ca190-dd59-455b-b145-18ff05a038f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]</value>
      <webElementGuid>68538f6d-780a-4b7f-87e3-1ac7ac184a7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'BCA KlikPay' or . = 'BCA KlikPay')]</value>
      <webElementGuid>3820fffe-ac99-486c-98db-d819291086b8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
