<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Wording - Akulaku Paylater</name>
   <tag></tag>
   <elementGuidId>bdeba0f4-87f8-4f40-9ff9-73606ab172cf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.payment-page-layout.payment-page-text</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3472fb4d-10fc-4d26-8083-b63ce427953a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>payment-page-layout payment-page-text</value>
      <webElementGuid>49dfd024-6f1a-450b-a79a-454763b58fb3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Akulaku PayLater allows you to shop in installments without using a credit card.</value>
      <webElementGuid>974ab5f2-5e62-4351-951f-af94db5dba7d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;payment-page-layout payment-page-text&quot;]</value>
      <webElementGuid>fb0f41d0-6e8f-4a8c-a21c-bda575368b67</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Website/Events/Checkout/iframe_concat(id(, , snap-midtrans, , ))_popup_1689793297466</value>
      <webElementGuid>9f947aad-6e4a-4b11-ab30-00b41e775798</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div[2]</value>
      <webElementGuid>4c8ae316-6d18-4564-9331-50e4f5c20183</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Akulaku PayLater'])[1]/following::div[1]</value>
      <webElementGuid>ed5c8e57-fad6-4767-94a2-f5f593031398</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='How to pay'])[1]/preceding::div[1]</value>
      <webElementGuid>5db925f0-17be-404f-9d09-4836b0cf355c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Akulaku PayLater allows you to shop in installments without using a credit card.']/parent::*</value>
      <webElementGuid>f5b0e9b7-1b99-4c26-97cf-e984b6cab4b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div[2]</value>
      <webElementGuid>813e4239-e55f-47e6-a35c-8fb7433c846f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Akulaku PayLater allows you to shop in installments without using a credit card.' or . = 'Akulaku PayLater allows you to shop in installments without using a credit card.')]</value>
      <webElementGuid>56f7171a-d12e-47cc-9a8c-719a3fb3db9a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
