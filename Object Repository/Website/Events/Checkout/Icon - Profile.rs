<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Icon - Profile</name>
   <tag></tag>
   <elementGuidId>18843d69-4684-479d-bf44-1bb735f1ee69</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>i.fas.fa-user-alt</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbar-collapse-1']/ul/li[7]/a/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>9c11ec9d-801a-4752-b723-04579722cae8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fas fa-user-alt</value>
      <webElementGuid>9678bdc5-33be-46c7-9e05-845b47cc0457</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbar-collapse-1&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;___class_+?26___&quot;]/a[1]/i[@class=&quot;fas fa-user-alt&quot;]</value>
      <webElementGuid>ed52c0e5-f03d-4d54-8a02-28fbafd82a6f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbar-collapse-1']/ul/li[7]/a/i</value>
      <webElementGuid>eb0bca2d-4a5f-43d1-91fa-2ee65b489e82</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/i</value>
      <webElementGuid>b1adc43c-19c1-4983-9003-f89fabba6a65</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
