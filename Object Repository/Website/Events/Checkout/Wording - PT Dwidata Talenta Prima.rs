<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Wording - PT Dwidata Talenta Prima</name>
   <tag></tag>
   <elementGuidId>8fbe115d-0556-4a7f-b3d6-6f80e2ba14e7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.merchant-name</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//nav[@id='header']/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f505b817-f906-4320-8615-8f899df2e897</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>merchant-name</value>
      <webElementGuid>b6856b14-e6f3-4701-9832-2a16ca604f75</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>PT Dwidata Talenta Prima</value>
      <webElementGuid>544a270f-5084-4ab0-8c0c-ab080a55192f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;header&quot;)/div[@class=&quot;title-bar&quot;]/div[@class=&quot;logo-store&quot;]/div[@class=&quot;merchant-name&quot;]</value>
      <webElementGuid>c50c9084-7340-4787-ba36-daa983a1a2e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Website/Events/Checkout/iframe_concat(id(, , snap-midtrans, , ))_popup_1689793297466</value>
      <webElementGuid>7db47a4e-aae0-4604-9070-4154eb00966b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//nav[@id='header']/div/div/div</value>
      <webElementGuid>c84d8d8f-f704-4b5a-b0e2-40e33a5c7e45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TEST'])[1]/following::div[5]</value>
      <webElementGuid>2a7a0a20-bcdf-4ae2-9222-dd8603d6b644</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total'])[1]/preceding::div[2]</value>
      <webElementGuid>c943ac0e-3f76-4ff3-a168-738adab302fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='PT Dwidata Talenta Prima']/parent::*</value>
      <webElementGuid>852b389e-f4cd-4b94-810d-2004fddb987c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//nav/div/div/div</value>
      <webElementGuid>42101aea-9711-4965-aebb-2a75d0d8cc39</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'PT Dwidata Talenta Prima' or . = 'PT Dwidata Talenta Prima')]</value>
      <webElementGuid>c2149cb6-1dde-42ab-8433-09a90152c62b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
