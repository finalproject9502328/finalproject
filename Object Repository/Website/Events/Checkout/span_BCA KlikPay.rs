<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_BCA KlikPay</name>
   <tag></tag>
   <elementGuidId>1a707ae1-94ff-4c9e-8bfb-74418a3800c7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.title-text.text-actionable-bold</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div/span[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>e54d8478-1035-4ba5-a3d7-050e66fc3dc8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>title-text text-actionable-bold</value>
      <webElementGuid>e3088f1a-2348-4e4b-9ac6-a4bd395295bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>BCA KlikPay</value>
      <webElementGuid>8cb4e6f1-1ba4-47e6-b824-03497390e8d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;page-title&quot;]/span[@class=&quot;title-text text-actionable-bold&quot;]</value>
      <webElementGuid>93aa7b60-4eef-45ca-b0dd-d8052f3a697b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Website/Events/Checkout/iframe_concat(id(, , snap-midtrans, , ))_popup_1689793297466</value>
      <webElementGuid>7e740d8d-155d-4f1b-b470-3d58b1b8519f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div/span[2]</value>
      <webElementGuid>4225c9f9-9f65-4564-bd15-17a8ca3f7bea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order details'])[1]/following::span[2]</value>
      <webElementGuid>b3217c52-4fe5-492c-8ff6-67bb147c5d94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please complete BCA KlikPay through BCA KlikPay website.'])[1]/preceding::span[2]</value>
      <webElementGuid>954609c9-a745-49c4-9fa7-65025e13cfbc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='How to pay'])[1]/preceding::span[2]</value>
      <webElementGuid>39f39056-329e-47ef-90b5-37e0da21d282</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='BCA KlikPay']/parent::*</value>
      <webElementGuid>2f61b5df-37ab-4576-bc2d-78fc5a3275b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]</value>
      <webElementGuid>69cd0524-981d-4ba9-9eeb-f2da6c366f31</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'BCA KlikPay' or . = 'BCA KlikPay')]</value>
      <webElementGuid>75cc2a66-b27e-4bde-be3e-58fc375cadef</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
