<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Wording - Indomaret</name>
   <tag></tag>
   <elementGuidId>24f86836-8726-4e88-8e46-06f8ea1786f8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.payment-page-layout.payment-page-text</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>abe96cec-cf00-437b-af2c-a848eb6d7ded</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>payment-page-layout payment-page-text</value>
      <webElementGuid>c5b6f37f-4811-4176-b1ea-c21cf173ec38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Show barcode to the cashier or enter payment code on i.saku app.</value>
      <webElementGuid>84be8e72-10c1-4a64-826a-00d6c2746569</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;payment-page-layout payment-page-text&quot;]</value>
      <webElementGuid>9cb361af-1e5b-42d4-bc56-c47879037133</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Website/Events/Checkout/iframe_concat(id(, , snap-midtrans, , ))_popup_1689793297466</value>
      <webElementGuid>9007b5ee-2f71-4d1f-a608-5ccd8fb65aec</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div[2]</value>
      <webElementGuid>91d1c0cd-4a1c-4825-b90e-33965142e4eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Indomaret'])[1]/following::div[1]</value>
      <webElementGuid>b9d0b2a5-7c31-4ad6-bfa0-f6819b6f5081</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment code'])[1]/preceding::div[1]</value>
      <webElementGuid>5c8c2300-5a95-45d8-bcef-06ab51bb7666</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy'])[1]/preceding::div[3]</value>
      <webElementGuid>da825b79-58ef-49fa-b136-7e3d5586f206</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Show barcode to the cashier or enter payment code on i.saku app.']/parent::*</value>
      <webElementGuid>15b0216c-1e3c-4cc5-b507-b2e5ff35e24c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div[2]</value>
      <webElementGuid>987b4cec-54a3-49e7-8fab-0be7b661365b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Show barcode to the cashier or enter payment code on i.saku app.' or . = 'Show barcode to the cashier or enter payment code on i.saku app.')]</value>
      <webElementGuid>259f9602-92dc-4e92-b6f6-4bfac9ef95f2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
