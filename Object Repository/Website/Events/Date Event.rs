<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Date Event</name>
   <tag></tag>
   <elementGuidId>866564f2-3775-4926-a8b9-e965b68fc67d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='blockListEvent']/a/div/div[2]/h6[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h6.eventTime</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h6</value>
      <webElementGuid>55d6c5ee-1705-44de-9d7a-bb3e9fba2c63</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>eventTime</value>
      <webElementGuid>3d664e8f-6fce-4258-a13d-fdee2f949d0d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                18 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            </value>
      <webElementGuid>a361d1d9-1f1c-4f18-bbac-1001a2aa4c46</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;loopevent&quot;)/li[@id=&quot;blockListEvent&quot;]/a[1]/div[@class=&quot;cardOuter&quot;]/div[@class=&quot;blockBody&quot;]/h6[@class=&quot;eventTime&quot;]</value>
      <webElementGuid>d0c69ac3-a4bd-4a70-ab87-33bf2c70fe43</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='blockListEvent']/a/div/div[2]/h6[2]</value>
      <webElementGuid>b0ffdeca-744d-4b9a-8280-d8238718e3f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Day 3: Predict using Machine Learning'])[2]/following::h6[1]</value>
      <webElementGuid>200d8e25-3a7c-4907-a9ed-33d94f51dc12</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mini Class'])[1]/following::h6[2]</value>
      <webElementGuid>7b665f5a-7d88-425b-8e37-ca7b49855a1f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h6[2]</value>
      <webElementGuid>dcb912a5-a9b4-490c-b30c-8c522bca55bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h6[(text() = '
                                                18 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            ' or . = '
                                                18 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            ')]</value>
      <webElementGuid>f07931cc-78ec-486a-8abd-e4b151e7ca5f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
