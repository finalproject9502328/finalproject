<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>alertPhotoSheet</name>
   <tag></tag>
   <elementGuidId>67987323-5cb2-4a3c-b0c8-d2f7feca74ad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.ui-exception-message</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='NotReadableException'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>1398db4c-f874-4bd4-8840-1a3e9c427c15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ui-exception-message</value>
      <webElementGuid>570365c2-2ed6-4aed-a33d-d524f55073cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    Unsupported image type application/vnd.openxmlformats-officedocument.spreadsheetml.sheet. GD driver is only able to decode JPG, PNG, GIF, BMP or WebP files.
</value>
      <webElementGuid>ba58adfa-1342-4e47-ad3d-c6faccdc5f45</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;theme-light&quot;]/body[@class=&quot;scrollbar-lg&quot;]/div[1]/div[1]/div[@class=&quot;layout-col z-10&quot;]/div[@class=&quot;mt-12 card card-has-header card-no-props&quot;]/div[@class=&quot;card-details&quot;]/div[@class=&quot;card-details-overflow scrollbar p-12 pt-10&quot;]/div[@class=&quot;text-2xl&quot;]/div[@class=&quot;ui-exception-message&quot;]</value>
      <webElementGuid>263b8856-9c51-47b5-8f2c-b392bec8a7c0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='NotReadableException'])[1]/following::div[1]</value>
      <webElementGuid>d5d14181-36cf-4871-bd1f-3dd5430275ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Exception\'])[1]/following::div[1]</value>
      <webElementGuid>6eb4f597-02e0-44cb-a0b5-588113c276cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='https://demo-app.online/dashboard/profile/update'])[1]/preceding::div[1]</value>
      <webElementGuid>32fd203a-88b8-4e12-a9ae-81215d870867</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Stack trace'])[1]/preceding::div[2]</value>
      <webElementGuid>a5bf7983-987f-4cdb-8315-15b225308c62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Unsupported image type application/vnd.openxmlformats-officedocument.spreadsheetml.sheet. GD driver is only able to decode JPG, PNG, GIF, BMP or WebP files.']/parent::*</value>
      <webElementGuid>1c80e782-21fa-4422-bc70-9be84121ab79</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div</value>
      <webElementGuid>fdb7b49e-301f-4f82-b927-fa845d1f66db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    Unsupported image type application/vnd.openxmlformats-officedocument.spreadsheetml.sheet. GD driver is only able to decode JPG, PNG, GIF, BMP or WebP files.
' or . = '
    Unsupported image type application/vnd.openxmlformats-officedocument.spreadsheetml.sheet. GD driver is only able to decode JPG, PNG, GIF, BMP or WebP files.
')]</value>
      <webElementGuid>a5d39f39-7866-4702-aa66-f7b4a0108b8f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
