<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>alertLargeFike</name>
   <tag></tag>
   <elementGuidId>17c080f6-1012-4faa-8a60-bdd37bd22c9a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.ui-exception-message.ui-exception-message-full</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='NotReadableException'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>d943b6a5-8bc1-44ee-9246-ef14201977e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ui-exception-message ui-exception-message-full</value>
      <webElementGuid>1d856553-dc7e-4f1a-b7af-244bb47add4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    Unsupported image type application/zip. GD driver is only able to decode JPG, PNG, GIF, BMP or WebP files.
</value>
      <webElementGuid>9727a987-b007-4ef6-b6ab-c1b328d2e1bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;theme-light&quot;]/body[@class=&quot;scrollbar-lg&quot;]/div[1]/div[1]/div[@class=&quot;layout-col z-10&quot;]/div[@class=&quot;mt-12 card card-has-header card-no-props&quot;]/div[@class=&quot;card-details&quot;]/div[@class=&quot;card-details-overflow scrollbar p-12 pt-10&quot;]/div[@class=&quot;text-2xl&quot;]/div[@class=&quot;ui-exception-message ui-exception-message-full&quot;]</value>
      <webElementGuid>57f81978-a054-4df7-b308-7b02a6d06566</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='NotReadableException'])[1]/following::div[1]</value>
      <webElementGuid>557c9e9f-a079-4ce3-9973-b82479c949c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Exception\'])[1]/following::div[1]</value>
      <webElementGuid>f4d4f4e0-df07-4d83-87d4-1debe588c414</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='https://demo-app.online/dashboard/profile/update'])[1]/preceding::div[1]</value>
      <webElementGuid>1d7470d4-cac8-4a9e-81ec-b671d2b6b252</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Stack trace'])[1]/preceding::div[2]</value>
      <webElementGuid>87d79419-1060-441c-8783-286bce4a6d96</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Unsupported image type application/zip. GD driver is only able to decode JPG, PNG, GIF, BMP or WebP files.']/parent::*</value>
      <webElementGuid>3a24801d-34fc-4eff-a3a4-c2bf9d4a0c7f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div</value>
      <webElementGuid>66bab977-c3d1-498b-a843-91c10148a0cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    Unsupported image type application/zip. GD driver is only able to decode JPG, PNG, GIF, BMP or WebP files.
' or . = '
    Unsupported image type application/zip. GD driver is only able to decode JPG, PNG, GIF, BMP or WebP files.
')]</value>
      <webElementGuid>570c5664-5cb2-44ae-89da-03f7c10a052a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
