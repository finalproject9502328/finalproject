<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>alertPhotoAudio</name>
   <tag></tag>
   <elementGuidId>8ef744c1-2a6a-497f-80ee-9d51ee34bf47</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.ui-exception-message</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='NotReadableException'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f51f37f7-0d33-4d8c-bf1a-6171b5a0b20f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ui-exception-message</value>
      <webElementGuid>7f0703e4-0d69-44e7-96eb-0a464dace44e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    Unsupported image type audio/mpeg. GD driver is only able to decode JPG, PNG, GIF, BMP or WebP files.
</value>
      <webElementGuid>3be192b9-d0ae-4a3a-83f4-2940337c47e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;theme-light&quot;]/body[@class=&quot;scrollbar-lg&quot;]/div[1]/div[1]/div[@class=&quot;layout-col z-10&quot;]/div[@class=&quot;mt-12 card card-has-header card-no-props&quot;]/div[@class=&quot;card-details&quot;]/div[@class=&quot;card-details-overflow scrollbar p-12 pt-10&quot;]/div[@class=&quot;text-2xl&quot;]/div[@class=&quot;ui-exception-message&quot;]</value>
      <webElementGuid>d5fa95c3-a063-4081-b07f-6bc1fa3f6f9a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='NotReadableException'])[1]/following::div[1]</value>
      <webElementGuid>22b0935d-e940-4d16-9eef-fcd56133b515</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Exception\'])[1]/following::div[1]</value>
      <webElementGuid>7f91ab22-a3a2-4253-8aee-6cfa525ef186</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='https://demo-app.online/dashboard/profile/update'])[1]/preceding::div[1]</value>
      <webElementGuid>4bd6a580-34b5-480b-8cc3-cdd51638c885</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Stack trace'])[1]/preceding::div[2]</value>
      <webElementGuid>46a93e56-5dc3-4767-8747-4995f43ffb26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Unsupported image type audio/mpeg. GD driver is only able to decode JPG, PNG, GIF, BMP or WebP files.']/parent::*</value>
      <webElementGuid>c085f483-7ef0-4e66-808d-19ada31a0c37</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div</value>
      <webElementGuid>1ac09359-45d8-4ba2-afb4-443b011afe69</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    Unsupported image type audio/mpeg. GD driver is only able to decode JPG, PNG, GIF, BMP or WebP files.
' or . = '
    Unsupported image type audio/mpeg. GD driver is only able to decode JPG, PNG, GIF, BMP or WebP files.
')]</value>
      <webElementGuid>c0939cae-e4f3-44f7-a3e1-0395ceaff0de</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
