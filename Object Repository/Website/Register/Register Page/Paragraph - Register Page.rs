<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Paragraph - Register Page</name>
   <tag></tag>
   <elementGuidId>5d3aa1e4-f52d-4fb2-850a-3e2c305ac8d7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Buat Akun Baru'])[1]/following::p[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.wm-fancy-title > p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>4b72aee7-921c-4325-849d-8812b593a61e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Daftarkan dirimu &amp; lengkapi pengetahuanmu bersama kami</value>
      <webElementGuid>f5aa7f3e-6a1c-4318-b689-e468c9470bae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-htmlimports no-flash no-proximity no-applicationcache blobconstructor blob-constructor cookies cors customprotocolhandler dataview eventlistener geolocation history no-ie8compat json notification queryselector serviceworker customevent postmessage svg templatestrings typedarrays websockets no-xdomainrequest webaudio webworkers no-contextmenu cssall audio canvas canvastext contenteditable emoji olreversed no-userdata video no-vml webanimations webgl adownload audioloop canvasblending todataurljpeg todataurlpng todataurlwebp canvaswinding no-ambientlight hashchange inputsearchevent pointerevents no-hiddenscroll mathml unicoderange no-touchevents no-unicode no-batteryapi no-battery-api crypto no-dart gamepads fullscreen indexeddb indexeddb-deletedatabase intl pagevisibility performance pointerlock quotamanagement requestanimationframe raf vibrate no-webintents no-lowbattery getrandomvalues backgroundblendmode cssanimations backdropfilter backgroundcliptext appearance exiforientation audiopreload&quot;]/body[1]/div[@class=&quot;wm-main-wrapper&quot;]/div[@class=&quot;wm-main-section&quot;]/div[@class=&quot;wm-fancy-title&quot;]/p[1]</value>
      <webElementGuid>ebbaa2fc-51c8-4f79-bfba-fa820a3ee1ea</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Buat Akun Baru'])[1]/following::p[1]</value>
      <webElementGuid>8c3519ce-322b-4c8a-a685-a84a543d2d36</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama'])[1]/preceding::p[1]</value>
      <webElementGuid>d5ef7f90-ec83-4abe-b0f3-48d2870b11ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tanggal lahir'])[1]/preceding::p[1]</value>
      <webElementGuid>1c96d3af-3e51-4e96-af65-f7428ecdd54f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Daftarkan dirimu &amp; lengkapi pengetahuanmu bersama kami']/parent::*</value>
      <webElementGuid>62253a73-d38e-4477-adb5-76e79a959f1f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/p</value>
      <webElementGuid>16f7cdb8-301c-49a7-9020-211d021df357</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = ' Daftarkan dirimu &amp; lengkapi pengetahuanmu bersama kami' or . = ' Daftarkan dirimu &amp; lengkapi pengetahuanmu bersama kami')]</value>
      <webElementGuid>0c01027c-8014-46a7-a346-83cee48a6dca</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
