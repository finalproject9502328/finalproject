<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Form_Nama</name>
   <tag></tag>
   <elementGuidId>5740dcc3-5554-45c4-91f7-0bc12d3ad506</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-md-6 > form</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@action='/daftar/verif']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>form</value>
      <webElementGuid>d8d8fe67-b2da-47bf-9141-bf75e7007a7b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>method</name>
      <type>Main</type>
      <value>post</value>
      <webElementGuid>100cfe1c-d22e-4dfb-89d5-56af804bd0fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>action</name>
      <type>Main</type>
      <value>/daftar/verif</value>
      <webElementGuid>eae1b12c-4544-4404-a43b-a9f60f23846c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                
                                
                                    Nama

                                    
                                        

                                                                            
                                
                                
                                    Tanggal lahir

                                    
                                        

                                                                            
                                


                                
                                    E-Mail

                                    
                                        

                                                                            
                                
                                
                                    Whatsapp

                                    
                                        

                                                                            
                                

                                
                                    Kata Sandi

                                    
                                        
                                                                            
                                

                                
                                    Konfirmasi kata sandi


                                    
                                        
                                                                            
                                
                                
                                    
                                        
                                        
                                            
                                                Saya setuju
                                                    dengan syarat dan ketentuan yang
                                                    berlaku
                                            
                                        
                                    
                                

                                
                                    
                                        
                                            Daftar
                                        
                                    
                                
                                
                                
                                     
                                            
                                            Sign-in With Google
                                        
                                

                                
                                    Sudah punya akun ?Silahkan
                                        Masuk
                                
                            </value>
      <webElementGuid>ee068646-527e-4d22-b0f5-07871f4b1e27</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-htmlimports no-flash no-proximity no-applicationcache blobconstructor blob-constructor cookies cors customprotocolhandler dataview eventlistener geolocation history no-ie8compat json notification queryselector serviceworker customevent postmessage svg templatestrings typedarrays websockets no-xdomainrequest webaudio webworkers no-contextmenu cssall audio canvas canvastext contenteditable emoji olreversed no-userdata video no-vml webanimations webgl adownload audioloop canvasblending todataurljpeg todataurlpng todataurlwebp canvaswinding no-ambientlight hashchange inputsearchevent pointerevents no-hiddenscroll mathml unicoderange no-touchevents no-unicode no-batteryapi no-battery-api crypto no-dart gamepads fullscreen indexeddb indexeddb-deletedatabase intl pagevisibility performance pointerlock quotamanagement requestanimationframe raf vibrate no-webintents no-lowbattery getrandomvalues backgroundblendmode cssanimations backdropfilter backgroundcliptext appearance exiforientation audiopreload&quot;]/body[@class=&quot;wm-sticky&quot;]/div[@class=&quot;wm-main-wrapper&quot;]/div[@class=&quot;wm-main-section&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-md-12 col-sm-12 col-xs-12&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6&quot;]/form[1]</value>
      <webElementGuid>d679763b-27e8-4a0c-979f-75182734534f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//form[@action='/daftar/verif']</value>
      <webElementGuid>60dfaf6a-3503-4b35-b609-ff451d1b821e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Buat Akun Baru'])[1]/following::form[1]</value>
      <webElementGuid>4ef6e136-9035-4a22-a1fb-b3858c7e46bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/form</value>
      <webElementGuid>5bba0139-fd7a-4243-a38d-e9e9c9577dc7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//form[(text() = '
                                
                                
                                    Nama

                                    
                                        

                                                                            
                                
                                
                                    Tanggal lahir

                                    
                                        

                                                                            
                                


                                
                                    E-Mail

                                    
                                        

                                                                            
                                
                                
                                    Whatsapp

                                    
                                        

                                                                            
                                

                                
                                    Kata Sandi

                                    
                                        
                                                                            
                                

                                
                                    Konfirmasi kata sandi


                                    
                                        
                                                                            
                                
                                
                                    
                                        
                                        
                                            
                                                Saya setuju
                                                    dengan syarat dan ketentuan yang
                                                    berlaku
                                            
                                        
                                    
                                

                                
                                    
                                        
                                            Daftar
                                        
                                    
                                
                                
                                
                                     
                                            
                                            Sign-in With Google
                                        
                                

                                
                                    Sudah punya akun ?Silahkan
                                        Masuk
                                
                            ' or . = '
                                
                                
                                    Nama

                                    
                                        

                                                                            
                                
                                
                                    Tanggal lahir

                                    
                                        

                                                                            
                                


                                
                                    E-Mail

                                    
                                        

                                                                            
                                
                                
                                    Whatsapp

                                    
                                        

                                                                            
                                

                                
                                    Kata Sandi

                                    
                                        
                                                                            
                                

                                
                                    Konfirmasi kata sandi


                                    
                                        
                                                                            
                                
                                
                                    
                                        
                                        
                                            
                                                Saya setuju
                                                    dengan syarat dan ketentuan yang
                                                    berlaku
                                            
                                        
                                    
                                

                                
                                    
                                        
                                            Daftar
                                        
                                    
                                
                                
                                
                                     
                                            
                                            Sign-in With Google
                                        
                                

                                
                                    Sudah punya akun ?Silahkan
                                        Masuk
                                
                            ')]</value>
      <webElementGuid>360421fd-9e41-4600-857f-8c2042b9fcb4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
