<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Paragraph - Check Email</name>
   <tag></tag>
   <elementGuidId>35a58c46-5814-43ac-8651-975f7883c991</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Verifikasi alamat email Anda'])[1]/following::div[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-md-12 > div.card > div.card-body</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>df5396d7-b2d2-4711-8906-d772f64eca6d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-body</value>
      <webElementGuid>dc702f9a-4622-4fe9-bf9d-502c57dd4df5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>


                                        Sebelum melanjutkan, periksa email Anda untuk link verifikasi
                                        Jika Anda tidak menerima email,
                                        Silahkan klik tombol di bawah,
                                         Tombol aktif dalam 48 Detik 

                                        
                                                                                        Kirim Ulang
                                        
                                    </value>
      <webElementGuid>46f8a355-db36-49d4-88ba-ef1f4f8e8bee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-htmlimports no-flash no-proximity no-applicationcache blobconstructor blob-constructor cookies cors customprotocolhandler dataview eventlistener geolocation history no-ie8compat json notification queryselector serviceworker customevent postmessage svg templatestrings typedarrays websockets no-xdomainrequest webaudio webworkers no-contextmenu cssall audio canvas canvastext contenteditable emoji olreversed no-userdata video no-vml webanimations webgl adownload audioloop canvasblending todataurljpeg todataurlpng todataurlwebp canvaswinding no-ambientlight hashchange inputsearchevent pointerevents no-hiddenscroll mathml unicoderange no-touchevents no-unicode no-batteryapi no-battery-api crypto no-dart gamepads fullscreen indexeddb indexeddb-deletedatabase intl pagevisibility performance pointerlock quotamanagement requestanimationframe raf vibrate no-webintents no-lowbattery getrandomvalues backgroundblendmode cssanimations backdropfilter backgroundcliptext appearance exiforientation audiopreload&quot;]/body[1]/div[@class=&quot;wm-main-wrapper&quot;]/div[@class=&quot;wm-main-section&quot;]/div[@class=&quot;container&quot;]/div[1]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-md-12&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]</value>
      <webElementGuid>fd74217d-0494-4b85-9219-4f3d895f745d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Verifikasi alamat email Anda'])[1]/following::div[1]</value>
      <webElementGuid>f82789e1-53b1-4ebf-ac0a-5154092854f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Verifikasi Email'])[1]/following::div[7]</value>
      <webElementGuid>f34af1d3-0f64-4a93-a2b6-7986bf2899cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About CODING.ID'])[1]/preceding::div[1]</value>
      <webElementGuid>e8a3a15b-5f95-418a-b782-a93a61b00220</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div[2]/div/div/div/div/div[2]</value>
      <webElementGuid>cf573c94-3fe4-41f8-89b2-17bccb8a9cfd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '


                                        Sebelum melanjutkan, periksa email Anda untuk link verifikasi
                                        Jika Anda tidak menerima email,
                                        Silahkan klik tombol di bawah,
                                         Tombol aktif dalam 48 Detik 

                                        
                                                                                        Kirim Ulang
                                        
                                    ' or . = '


                                        Sebelum melanjutkan, periksa email Anda untuk link verifikasi
                                        Jika Anda tidak menerima email,
                                        Silahkan klik tombol di bawah,
                                         Tombol aktif dalam 48 Detik 

                                        
                                                                                        Kirim Ulang
                                        
                                    ')]</value>
      <webElementGuid>d33729da-cb07-4ec3-b83f-336356a8e71c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
