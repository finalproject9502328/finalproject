<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnLogin</name>
   <tag></tag>
   <elementGuidId>889f08f8-17c6-4802-b708-6276ab5afa07</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#buttonLoginTrack</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='buttonLoginTrack']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>62121df8-3495-4310-8369-8b65660f8cb3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>buttonLoginTrack</value>
      <webElementGuid>49c6e736-946b-4bf3-9e4a-2c4480cd5b4c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>cf2d05d6-91ef-4e6f-adc9-07c85a090548</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary btn-lg btn-block</value>
      <webElementGuid>3b879904-eb6f-4732-981e-4f9b9b0ca4ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>4</value>
      <webElementGuid>8c2f63ff-e3fb-4165-8b4d-a9a097cd86f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                        Login
                                                    </value>
      <webElementGuid>0b89956b-0e88-4805-84a9-8842f2470625</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;buttonLoginTrack&quot;)</value>
      <webElementGuid>1fa1107b-2b45-4214-95e6-20f32f6830d3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='buttonLoginTrack']</value>
      <webElementGuid>bc5b64f4-f67a-4331-b7de-9b82cc19fec2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[1]/following::button[1]</value>
      <webElementGuid>c7516c88-b0fc-4220-8c14-cc10d6c2b2c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lupa kata sandi ?'])[1]/preceding::button[2]</value>
      <webElementGuid>a15b9e46-358b-4c70-82d9-79bc4c979107</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/button</value>
      <webElementGuid>7a7f9252-f5d5-429e-ac19-5fde54d6bd3f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'buttonLoginTrack' and @type = 'submit' and (text() = '
                                                        Login
                                                    ' or . = '
                                                        Login
                                                    ')]</value>
      <webElementGuid>4dcfe7b4-39e3-4eda-9e5f-38bc4c19d005</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
