import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Monica Cindy/Mobile/Register/Positive/RG001 - Succeed Open The Register Menu'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Mobile/Register/tapNama'), 0)

Mobile.sendKeys(findTestObject('Mobile/Register/inputNama'), 'TEAM EMPAT')

Mobile.tap(findTestObject('Mobile/Change Profile/btnCalender'), 0)

Mobile.tap(findTestObject('Mobile/Register/tapTahun'), 0)

Mobile.tap(findTestObject('Mobile/Register/pilihTahun'), 0)

Mobile.tap(findTestObject('Mobile/Register/tapRowRight'), 0)

Mobile.tap(findTestObject('Mobile/Register/tapTanggal'), 0)

Mobile.tap(findTestObject('Mobile/Register/btnOK'), 0)

Mobile.tap(findTestObject('Mobile/Register/tapEmail'), 0)

Mobile.sendKeys(findTestObject('Mobile/Login/inputEmail'), 'teamempat@gmail.com')

Mobile.tap(findTestObject('Mobile/Register/tapWhatsapp'), 0)

Mobile.sendKeys(findTestObject('Mobile/Register/inputWhatsapp'), '089123456789')

Mobile.tap(findTestObject('Mobile/Register/tapKataSandi'), 0)

Mobile.sendKeys(findTestObject('Mobile/Register/inputKataSandi'), 'semangat1')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Mobile/Register/tapUlangiKataSandi'), 0)

Mobile.sendKeys(findTestObject('Mobile/Register/inputUlangKataSandi'), 'team4')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Mobile/Register/tapCheckBox'), 0)

Mobile.tap(findTestObject('Mobile/Register/btnDaftar'), 0)

Mobile.verifyElementVisible(findTestObject('Mobile/Register/alertUlangiKataSandi'), 0)

