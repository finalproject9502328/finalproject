import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Malia Arismaya/Website/Register/Positive/RG001 - Succeed Open The Register Menu'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Website/Register/Register Page/Form - Name'), 'Malia Arismaya')

WebUI.setText(findTestObject('Website/Register/Register Page/Form - Birthdate'), '15-May-1999')

WebUI.setText(findTestObject('Website/Register/Register Page/Form - Email'), 'malia@gmail.com')

WebUI.setText(findTestObject('Website/Register/Register Page/Form - WhatsApp'), '')

WebUI.setEncryptedText(findTestObject('Website/Register/Register Page/Form - Password'), 'RigbBhfdqOBGNlJIWM1ClA==')

WebUI.setEncryptedText(findTestObject('Website/Register/Register Page/Form - Confirmation Password'), 'RigbBhfdqOBGNlJIWM1ClA==')

WebUI.click(findTestObject('Website/Register/Register Page/Checkbox - Agreed with Term Conditions'))

WebUI.click(findTestObject('Website/Register/Register Page/Button Register'))



WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Website/Register/Register Page/Form - WhatsApp'), 'validationMessage'), 'Please fill out this field.')

