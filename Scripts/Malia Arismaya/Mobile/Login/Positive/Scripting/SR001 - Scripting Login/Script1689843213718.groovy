import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

TestData testData=findTestData('Data Files/Mobile/DT001 - Spripting Login')

WebUI.callTestCase(findTestCase('Malia Arismaya/Mobile/Login/Positive/LG001 - Succeed Open The Login Menu'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Mobile/Login/Form - Email'), 0)

for(rowData = 1; rowData <= testData.getRowNumbers(); rowData++) {
	String email = testData.getValue(1, rowData)
	String password = testData.getValue(2, rowData)
	
	Mobile.sendKeys(findTestObject('Mobile/Login/Form - Email'), email)
	
	Mobile.tap(findTestObject('Mobile/Login/Form - Password'), 0)
	
	Mobile.sendKeys(findTestObject('Mobile/Login/Form - Password'), password)
	
	Mobile.tap(findTestObject('Mobile/Login/Button - Login'), 0)
	
	if(email == 'maliaarismayaichsanputri5@gmail.com' && password == '12345678') {
		Mobile.verifyElementVisible(findTestObject('Mobile/Login/btnCart'), 0)
	}
	else if(email == 'malia' && password == '12345678') {
		Mobile.verifyElementVisible(findTestObject('Mobile/Login/Pop up - Information'), 0)
		
		Mobile.verifyElementVisible(findTestObject('Mobile/Login/Pop up - Invalid Credential'), 0)
		
		Mobile.verifyElementVisible(findTestObject('Mobile/Login/Button - Ok'), 0)
		
		Mobile.tap(findTestObject('Mobile/Login/Button - Ok'), 0)
		
		Mobile.clearText(findTestObject('Mobile/Login/Form - Email'), 0)
		Mobile.clearText(findTestObject('Mobile/Login/Form - Password'), 0)
	}
	else if(email == 'maliaarismayaichsanputri5@gmail.com' && password == 'ngasalnih') {
		Mobile.verifyElementVisible(findTestObject('Mobile/Login/Pop up - Information'), 0)
		
		Mobile.verifyElementVisible(findTestObject('Mobile/Login/Pop up - Invalid Credential'), 0)
		
		Mobile.verifyElementVisible(findTestObject('Mobile/Login/Button - Ok'), 0)
		
		Mobile.tap(findTestObject('Mobile/Login/Button - Ok'), 0)
		
		Mobile.clearText(findTestObject('Mobile/Login/Form - Email'), 0)
		Mobile.clearText(findTestObject('Mobile/Login/Form - Password'), 0)
	}
	else {
		Mobile.verifyElementVisible(findTestObject('Mobile/Login/Pop up - Information'), 0)
		
		Mobile.verifyElementVisible(findTestObject('Mobile/Login/Pop up - Invalid Credential'), 0)
		
		Mobile.verifyElementVisible(findTestObject('Mobile/Login/Button - Ok'), 0)
		
		Mobile.tap(findTestObject('Mobile/Login/Button - Ok'), 0)
	}
}





